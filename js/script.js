var pomodoroLink = document.querySelector('#pomodoro-link');
var shortTimeLink = document.querySelector('#short-time-link');
var longTimeLink = document.querySelector('#long-time-link');
var timerDisplay = document.querySelector('.timer__display');
var signLink = document.querySelector('.sign-link');
var btnStart = document.querySelector('#btn-start');
var btnStop = document.querySelector('#btn-stop');
var btnReset = document.querySelector('#btn-reset');

var seconds = 25*60;
var pomodoroTime = 25*60;
var shortTime = 5*60;
var longTime = 10*60;

function start() {
    window.timerId = window.setInterval(tick, 1000);
}
function stop() {
    window.clearInterval(window.timerId);
}
function addZero(num) {
    if( num <= 9 ) {
        return '0' + num;
    } else {
        return num;
    }
}
function convertToMinutes(sec) {
    const minutes = Math.floor(sec / 60);
    const remainSeconds = sec % 60;
    return this.addZero(minutes) + ':' + this.addZero(remainSeconds);
}
function showTime(seconds) {
    timerDisplay.innerHTML = convertToMinutes(seconds);
}
function tick() {
    showTime(pomodoroTime);
    pomodoroTime = pomodoroTime - 1;
}

showTime(pomodoroTime);
btnStart.addEventListener('click', start);
btnStop.addEventListener('click', stop);